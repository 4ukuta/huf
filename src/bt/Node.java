package bt;

public class Node {
	int charNode;
	int weightNode;
	String huffCode;
	Node leftNode;
	Node rightNode;
	Node parentNode;
	
	//constructor root
	Node()
	{
		this.charNode = -1;
		this.weightNode = 0;
		this.huffCode = "1";
		this.leftNode = null;
		this.rightNode = null;
		this.parentNode = null;
	}
	
	//constructor left node
	Node(int kar, Node parent)
	{
		this.charNode = kar;
		this.weightNode = 1;
		this.huffCode = parent.huffCode + "0";
		this.leftNode = null;
		this.rightNode = null;
		this.parentNode = parent;
	}
	
	//constructor right node
	Node(Node parent)
	{
		this.charNode = -1;
		this.weightNode = 0;
		this.huffCode = parent.huffCode + "1";
		this.leftNode = null;
		this.rightNode = null;
		this.parentNode = parent;
	}
       
        public int updateWeight()
        {
            if(rightNode == null && leftNode == null)
            {
                return weightNode;
            }
            else{
                int weight = 0;
                if(rightNode != null)
                    weight+=rightNode.updateWeight();
                if(leftNode != null)
                    weight+= leftNode.updateWeight();
                weightNode = weight;
                return weightNode;
            }
        }
        
}

