package bt;

import java.util.*;

public class DynamicHuffman {
	
	final int MAX_CHAR = 65536; 
	String[] huffCode = new String[MAX_CHAR]; // for save the huffman code for each charater
	char[] charDat; // buffer
	
	Node root;
	Node atr;
	Node ptr;
	
	String compData="";
	
        public ArrayList<ArrayList<NodeForDraw>> stateMatrix = new ArrayList<ArrayList<NodeForDraw>>();
        
       
        
	public String initialData(String text) {
                        String outString = "";
			charDat = new char[(int) text.length()];
			
			root = new Node();
			atr = root;
			ptr = root;
			
			for ( int i = 0; i < text.length(); i++) {
				int kar = (int)text.charAt(i);
				charDat[(int) i] = (char) kar; //save character to buffer
				
				if (huffCode[kar] == null) {
					addNewNode(kar, atr);
                                        String sss = huffCode[kar];
                                        outString += huffCode[kar].substring(1, huffCode[kar].length()-1);
                                        outString += (char)kar;
				} else {
					ptr = root;
					updateNode(kar, ptr);
                                        outString += huffCode[kar].substring(1, huffCode[kar].length());
				}
                                recalculateWeights();
                                stateMatrix.add(getBinaryHeap());
                        }
                        Node asd = root;
                        return outString;
	}
	
	private void updateNode(int kar, Node ptr2) {
		//System.out.println("update node");
		if (ptr2.leftNode.charNode == kar) {
			ptr2.leftNode.weightNode += 1;
			
			if (ptr2 != root) {
				if (ptr2.leftNode.weightNode > ptr2.parentNode.leftNode.weightNode) {
					swapNode(ptr2);
				}
			}
		} else {
			updateNode(kar, ptr2.rightNode);
		}
                
	}
        
        private void recalculateWeights()
        {
            root.updateWeight();
        }
	
	private void swapNode(Node ptr2) {
		if (ptr2 != root) {
			if (ptr2.leftNode.weightNode > ptr2.parentNode.leftNode.weightNode) {
				Node temp = ptr2.leftNode;
				
				ptr2.leftNode = ptr2.parentNode.leftNode;
				ptr2.leftNode.parentNode = ptr2;
				ptr2.leftNode.huffCode = ptr2.huffCode + "0";
				huffCode[ptr2.leftNode.charNode] = ptr2.leftNode.huffCode;
				
                                
				ptr2.parentNode.leftNode = temp;
				ptr2.parentNode.leftNode.parentNode = ptr2.parentNode;
				ptr2.parentNode.leftNode.huffCode = ptr2.parentNode.huffCode
						+ "0";
				huffCode[ptr2.parentNode.leftNode.charNode] = ptr2.parentNode.leftNode.huffCode;
				
				swapNode(ptr2.parentNode);
			}
		}
	}
	
	private void addNewNode(int kar, Node atr2) {
		Node newLeaf = new Node(kar, atr2);
		Node newATR = new Node(atr2);
		
		atr2.leftNode = newLeaf;
		atr2.rightNode = newATR;
		
		atr = newATR;
		huffCode[kar] = newLeaf.huffCode;
	}
        
        public ArrayList getBinaryHeap(){
            ArrayList<NodeForDraw> list = new ArrayList<NodeForDraw>();
            int num = 0;
            Node node = root;
            LinkedList<Node> queue = new LinkedList<Node>();
            queue.add(node);
            while(!queue.isEmpty()){
               Node n = queue.remove();
               if(n!=null){
                   list.add(new NodeForDraw(n.weightNode, (char)n.charNode,num));
                    num++;
                   queue.add(n.rightNode);
                   queue.add(n.leftNode);
               }
               else{
                   list.add(new NodeForDraw(num));
                    num++;
               }
              
                
            }
            return list;
        }
	
	
	public static void main(String[] args) {
		DynamicHuffman dh = new DynamicHuffman();
                String s = "KOT";
               
		
                String s1 = dh.initialData(s);
	}
}