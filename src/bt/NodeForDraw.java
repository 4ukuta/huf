package bt;

/**
 *
 * @author ri
 */
public class NodeForDraw {
    public int weight;
    public char character;
    public int num;
    public NodeForDraw(int w, char ch,int n) {
        weight = w;
        character = ch;
        num = n;
    }

    public NodeForDraw(int n) {
        weight = 0;
        character = 0;
        num = n;
    }
    
    
    
}
